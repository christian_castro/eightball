function gerarNumeroAleatorio(maxNumber) {
    return Math.floor(Math.random() * maxNumber)
}


let botaoJogar = document.getElementById("botao")
let botaoLimpar = document.getElementById("botaoLimpar")
let respostaJogo = document.getElementById("resposta")
let valorTextArea = document.getElementById("valorTextArea")

botaoJogar.addEventListener("click", jogar)



function jogar() {
    let randonNumero = gerarNumeroAleatorio(4)

    let respostaTexto = ""

    if(randonNumero === 0){
        respostaTexto = "Sim, com certeza!"

    } else if(randonNumero === 1) {
        respostaTexto = "Me pergunte depois"

    } else if(randonNumero === 2) {
        respostaTexto = "Com certeza não!"

    } else {
        respostaTexto = "Eu não ligo para isso"
    }

    respostaJogo.innerHTML = respostaTexto
}


botaoLimpar.addEventListener('click', () => {
    respostaJogo.innerHTML = ""
    valorTextArea.value = ""
})
